const { func } = require("prop-types");

// JAVASCRIPT – If-Else
if(condition)
{
  statements;
}
else if(condition)
{
  statements;
}
else
{
  statements;
}

// JAVASCRIPT – Switch-Case
switch (expression) {
    case A : statement(s)
    break;
    
    case B: statement(s)
    break;
    
    case C: statement(s)
    break;
    
    default: statement(s)
 }

//  JAVASCRIPT – While Loop 

while(i<10){
    text+="The Number is " +1
    i++
}

//  JAVASCRIPT –  Do While Loop 

let text='';
let i=0;
do{
    text+=i + "<br>";
    i++;

}while(i<5);

// JAVASCRIPT – For Loop 

for (let index = 0; index < array.length; index++) {
    const element = array[index];
}

// JAVASCRIPT – For-in Loop

for (const key in object) {
    if (Object.hasOwnProperty.call(object, key)) {
        const element = object[key];
    }
}
const person = {fname:"John", lname:"Doe", age:25};
let text=''
for ( let x in person){
    text+=person[x]
}

// JAVASCRIPT – Loop Control

let x= 1 ;
// The break Statement
while(x<10){
    if(x==5){
        break; // breaks out of loop completely
    }
    x = x + 1;
}

// The continue Statement

while(x<10){
    x = x + 1;
    if(x==5){
        continue; // skip rest of the loop body
    }
}


// JAVASCRIPT – Functions

function myFunc(){
    return 'Hello world'
}

function multiply(a, b) {
    return a * b;
  }
  multiply(10, 2);

function addSquares(a,b){
    function square(x){
        return x * x
    }
    return square(a) + square(b)
}
addSquares(2,4)  // 20 


function Person (first, last){
    this.firstName=first,
    this.lastName=last
}
const myName=new Person('Amir','Mehmood')

console.log('My Name is ' + myName.firstName + myName.lastName)


var func = function(x,y) { 
    return x*y 
 };